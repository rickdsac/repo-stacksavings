({
    appDir: './src/',
    baseUrl: './js',
    dir: './products',
    modules: [
        {
            name: 'main'
        }
    ],
    fileExclusionRegExp: /^(r|build)\.js$/,
    optimizeCss: 'standard',
    removeCombined: true
    // paths: {
    //     jquery: 'jquery.min',
    // }
})