var http = require('http');
var csv = require("./node_modules/fast-csv/");

var fs = require("fs");
var path = require('path');

var async = require('async');
var socketio = require('socket.io');
var express = require('express');
var knox = require('knox');


// Creates a new instance of SimpleServer with the following options:
//  * `port` - The HTTP port to listen on. If `process.env.PORT` is set, _it overrides this value_.
//
var router = express();
var server = http.createServer(router);
var io = socketio.listen(server);

router.use(express.static(path.resolve(__dirname, 'client')));
var messages = [];
var sockets = [];



var util = require('util'),
  OperationHelper = require('apac').OperationHelper;

var opHelper = new OperationHelper({
  awsId: 'AKIAINDA6C3XEHC7726Q',
  awsSecret: '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',
  assocId: 'ricdandev-20'
    // xml2jsOptions: an extra, optional, parameter for if you want to pass additional options for the xml2js module. (see https://github.com/Leonidas-from-XIV/node-xml2js#options)
});


var product_field_name_mappings = {

  'Product Description': function(product) {
    var itemAttributes = product['ItemAttributes']
    var features = itemAttributes[0]['Feature']
    var description = ''
    var i = 0
    for (var feature in features) {
      description = description + ' ' + features[i]
      i++
    }
    return description
  },

  'Product Name': function(product) {
    return product.ItemAttributes[0].Title[0]

  },

  'gtin': function(product) {
    return ''
  },

  'Category': function(product) {

    return product.ItemAttributes[0].ProductGroup[0]

  },

  'Product UPC/EAN': function(product) {

    return product.ItemAttributes[0].EAN[0]

  },

  'Product Height': function(product) {


    var strValue = product.ItemAttributes[0].PackageDimensions[0].Height[0]["_"]
    var numValue = parseInt(strValue) / 100
    return numValue

  },

  'Product Width': function(product) {
    var strValue = product.ItemAttributes[0].PackageDimensions[0].Width[0]["_"]
    var numValue = parseInt(strValue) / 100
    return numValue

  },

  'Product Depth': function(product) {

    var strValue = product.ItemAttributes[0].PackageDimensions[0].Length[0]["_"]
    var numValue = parseInt(strValue) / 100
    return numValue
  },

  'Product Weight': function(product) {
    var strValue = product.ItemAttributes[0].PackageDimensions[0].Weight[0]["_"]
    var numValue = parseInt(strValue) / 10
    return numValue
  },

  'Product Image File - 1': function(product) {


    var img = ''

    var imgSets = []
    imgSets = product.ImageSets[0].ImageSet
      //console.log(imgSets)

    //console.log('new set')

    imgSets.forEach(function(iset) {
      img = iset.LargeImage[0].URL[0]
        //img = img+ iset.LargeImage[0].URL[0]+',' 
        //console.log(iset.LargeImage)
    })

    return img

    //console.log(product.ImageSets[0])
  },

  'Item Type': function(product) {


    return product.ItemAttributes[0].ProductGroup[0]

  },

  'Product Condition': function(product) {
    return 'New'
  },

  'Price': function(product) {

    return product.ItemAttributes[0].ListPrice[0].FormattedPrice[0]

  },

  'Product Availability': function(product) {


    return product.Offers[0].Offer[0].OfferListing[0].Availability[0]


  },

  'Expiration Date': function(product) {
    return ''
  },
  'Product Visible?': function(product) {
    return 'yes'
  },
  'Allow Purchases?': function(product) {
    return 'yes'
  },
  'Current Stock Level': function(product) {
    return 5
  }

}

io.on('connection', function(socket) {
  messages.forEach(function(data) {
    socket.emit('message', data);
  });

  sockets.push(socket);

  socket.on('disconnect', function() {
    sockets.splice(sockets.indexOf(socket), 1);
  });


  socket.on('runQuery', function(msg) {


    var inputLines = msg.split('\n')

    //write output
    var outputRows = []

    var statusArr = []

    var inputLineLength = inputLines.length
    var csvWriteFunction = function() {
      if (statusArr.length >= inputLineLength) {
        var ws = fs.createWriteStream("product_import.csv");
        csv
          .write(outputRows, {
            headers: true
          })
          .pipe(ws);
      }
    }

    var headerRow = []
      //Print column names as first row
    for (var fieldName in product_field_name_mappings) {
      headerRow.push(fieldName)
    }
    outputRows.push(headerRow)


    inputLines.forEach(function(line, index) {
      try {
        searchAmazonByKeyword(socket, opHelper, line, outputRows, statusArr, csvWriteFunction)
      }
      catch (err) {
        console.log(err)
      }
    })

  });

});



function searchAmazonByKeyword(socket, opHelper, keyword, outputRows, statusArr, csvWriteFunction) {


  opHelper.execute('ItemSearch', {
    'SearchIndex': 'All',
    'Keywords': keyword,
    'ResponseGroup': 'Large'
  }, function(err, results) {

    if (results === undefined || results.ItemSearchResponse === undefined) {
      statusArr.push(false)
      return
    }

    var list = results.ItemSearchResponse.Items[0]
    var products = []

    var stop = 1
    var i = 0
    while (i < stop) {
      if (list !== undefined && list.Item !== undefined) {
        var curProd = list.Item[i]
        products.push(curProd)
      }
      i++
    }

    if (products === undefined) {
      statusArr.push(false)
      return
    }
    
    var curProduct = products[0]
    
    if (curProduct === undefined) {
      statusArr.push(false)
      return
    }

    var outputRow = []
    for (var prop in product_field_name_mappings) {
      var anonymousFunction = product_field_name_mappings[prop]
      try {
        var value = anonymousFunction(curProduct)
        outputRow.push(value)
      }
      catch (err) {
        outputRow.push('')
        console.log(err)
      }
    }
    outputRows.push(outputRow)

    // socket.get('name', function(err, name) {
    //   var data = {
    //     name: name,
    //     text: outputRows
    //   };

    //   broadcast('message', 'Processed');
    //   messages.push(data);
    // });

    statusArr.push(true)
    csvWriteFunction()
  });
}


function broadcast(event, data) {
  sockets.forEach(function(socket) {
    socket.emit(event, data);
  });
}

server.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function() {
  var addr = server.address();
  console.log("Server listening at", addr.address + ":" + addr.port);
});