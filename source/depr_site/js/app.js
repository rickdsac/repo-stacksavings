define([
], function($, _, Backbone, Router){
  var initialize = function(){
    // Pass in our Router module and call it's initialize function
    Router.initialize();
    
    // requirejs(['css!../../css/custom.css'],function(css1){
    //     //css loaded, but I cannot be sure that they are already parsed by browser.
    // });
    
    
    
  }

  return {
    initialize: initialize
  };
});

