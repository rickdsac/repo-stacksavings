$(document).ready(function() {  
	
     // //Enable swiping...
     // $("body").swipe( {
     //   //Generic swipe handler for all directions
     //   swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
        	
        	
     //     $(this).text("You swiped.... " + direction );  
          
  			// heap.track('Swipe', {swipe_direction: direction});

     //   },
     //   //Default is 75px, set to 0 for demo so any distance triggers swipe
     //    threshold:0
     // });

	//Enable swiping...
	$("body").swipe( {
		//Generic swipe handler for all directions
		swipeLeft:function(event, direction, distance, duration, fingerCount) {
			//$("#left carousel-control").trigger("click");
			$(this).parent().carousel('prev'); 
			heap.track('SwipeLeft', {swipe_direction: "left"});
		},
		swipeRight: function() {
			//$("#right carousel-control").trigger("click");
			$(this).parent().carousel('next'); 
			heap.track('SwipeRight', {swipe_direction: "right"});
		},
		//Default is 75px, set to 0 for demo so any distance triggers swipe
		threshold:0
	});
	
	$(window).scroll($.debounce( 2000, true, function(){
	    heap.track('ScrollUp', {swipe_direction: "left"});
	}));
	$(window).scroll($.debounce( 2000, function(){
	    heap.track('ScrollDown', {swipe_direction: "left"});
	}));
	
	//product image click
	$(".slide_container").click(function(){
	  heap.track('ClickSliderImage', {swipe_direction: "left"});
	});
	
	//Click on search box
	$(".search-box").click(function(){
   		heap.track('Search Box', {elem_click: "true"});	
  	});
	
	//Tap on whole page	
	$(document).on("tap",function(){
    	heap.track('Tap', {tap: "true"});	
	}); 
		
	//Add to cart
	$("#addToCartButton").click(function(){
   		heap.track('addToCartButton', {elem_click: "true"});	
  	});
	$("#addToCartAmazonLogo").click(function(){
   		heap.track('addToCartAmazonLogo', {elem_click: "true"});	
  	});
	$("#cartLink").click(function(){
   		heap.track('cartLink', {elem_click: "true"});	
  	});
					
});



// Page is fully loaded
$(window).load(function() {
	//alert("page load complete");
	heap.track('Page Load', {page_load: "complete"});	
});

// User left page
//$(window).bind('beforeunload', function(){
//  return 'Are you sure you want to leave?';
//});
$(window).on('unload', function(){
	//alert("goodbye");
	heap.track('Page UnLoad', {page_unload: "true"});
});



