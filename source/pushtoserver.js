var knox = require('knox');
var walk = require('walk');
var fs = require('fs')

var destinationDirectory = 'products'
var delay = 7000 //delay to waite while deleting files on s3, as it's asynch
var runUploader = true
var runDeleter = true
var pathToReplace = "/home/ubuntu/workspace/site/public"
var replaceWith = ""

var pushDirectory = '/home/ubuntu/workspace/site/public'

if (!runDeleter) {
  delay = 0
}

//Generate any new pages, such as creating duplicate landing pages for different traffic sources
function generatePages(controlFlow) {
  try {
    controlFlow('start', 'generatePages')
    var fs = require('fs');

    var numberOfCopies = 10
    var i = 1
    while (i <= 10) {
      fs.createReadStream('src/product_chromebook.html').pipe(fs.createWriteStream(destinationDirectory + '/product_chromebook_' + i + '.html'));
      controlFlow('generate page', 'generatePages')
      i++
    }
  }
  catch (e) {
    controlFlow('error', 'generatePages', e)
  }

  controlFlow('finish', 'generatePages')
}

function resizeImages() {

  //TODO get all images from parsing file

  //TODO iterate through and resize each

  require('lwip').open('image.jpg', function(err, image) {

    image.batch()
      .scale(0.75) // scale to 75%
      .rotate(45, 'white') // rotate 45degs clockwise (white fill)
      .crop(200) // crop a 200X200 square from center
      .blur(5) // Gaussian blur with SD=5
      .writeFile('output.jpg', function(err) {

      });

  });
}

function uploadFullSiteToS3(controlFlow) {

  var client = knox.createClient({
    key: 'AKIAJI7252FE66S6YSLQ',
    secret: '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',
    bucket: 'sac-tech'
  });

  var object = {
    foo: "bar"
  };
  var string = JSON.stringify(object);


  var uploader = function(root, stat, next, controlFlow) {

    try {
      var fullPath = root + '/' + stat.name
      var fileName = stat.name
      var s3Path = fullPath.replace(pathToReplace, replaceWith)

      var req = client.put(fileName, {
        'Content-Length': string.length,
        'Content-Type': 'application/json',
        'x-amz-acl': 'public-read'
      });

      client.putFile(fullPath, s3Path, function(err, res) {
        try {
          res.resume()
        }
        catch (e) {

        }
      });


      req.on('response', function(res) {
        if (200 == res.statusCode) {

          console.log(s3Path + ",");
        }
      });
      req.end(string);

    }
    catch (e) {}
  }


  var deleter = function(controlFlow) {
    controlFlow
    try {
      controlFlow('start', 'deleter')
      client.list({}, function(err, data) {

        //delete existing files in the S3 bucket
        data['Contents'].forEach(function(entry) {
          client.deleteFile(entry['Key'], function(err, res) {
            try {
              res.resume()
            }
            catch (e) {

            }

          });
        });

      });
      controlFlow('finish', 'deleter')
    }
    catch (e) {
      controlFlow('error', 'deleter', e)
    }
  }

  if (runDeleter) {
    deleter(controlFlow)
  }


  //There are issues with figuring out when the deleter actually finished, so arbitrarily rate for a number of seconds as that should be enough time for it to finish
  console.log('Begin pause to wait for deleter to finish');
  var pause = true
  setTimeout(function() {

    console.log('End pause to wait for deleter to finish');

    if (runUploader) {
      try {
        controlFlow('start', 'walk')
        var files = [];

        var s3Delay = 550

        // Walker options
        var walker = walk.walk(pushDirectory, {
          followLinks: false
        });

        walker.on('file', function(root, stat, next) {

          uploader(root, stat, next, controlFlow)

          setTimeout(function() {
            next();
          }, s3Delay)
        });

        walker.on('end', function() {
          controlFlow('finish', 'walk')
        });
      }
      catch (e) {
        controlFlow('error', 'walk', e)
      }
    }

  }, delay);

}

var controlFlowFn = function() {
  var block = false

  var fn = function(action, operation, error) {


    if (block === false && action.toLowerCase() === 'start') {
      block = true
    }
    else if (action.toLowerCase() === 'start') {
      while (block) {}
      block = true
    }


    if (action.toLowerCase() === 'finish') {
      block = false
    }

    console.log('Action: ' + action + ' for operation: ' + operation)
    if (error) {
      block = false
      console.log('Error: ' + error)
    }
  }

  return fn
}

function rewriteUrls() {
  try {
    var files = [];

    var s3Delay = 550

    // Walker options
    var walker = walk.walk(pushDirectory, {
      followLinks: false
    });

    walker.on('file', function(root, fileStats, next) {

      var fullPath = root + '/' + fileStats.name

      if (fileStats.name.indexOf(".html") > -1) {

        fs.readFile(fullPath, 'utf8', function(err, data) {

          if (err) {
            return console.log(err);
          }


          var array = data.toString().split("\n");
          var output = ""
          for (var i in array) {
            
            var line = array[i]

            var regex = "<a.*?href=[\'|\"](.*?)[\'|\"]"

            var globalRegex = new RegExp(regex, 'g');
            var globalMatch = line.match(globalRegex);

            if (globalMatch !== undefined && globalMatch !== null && globalMatch.length > 0) {
              var token = globalMatch[0]
              
              if (token === undefined) {
                continue;
              }
              
              var re = new RegExp(token, "g");
              
              var strEnd = token.substring(token.length - 1, token.length) 

              var replaceWith = token.substring(0, token.length - 1) + "/index.html" + strEnd;
                            
              line =  line.replace(re, replaceWith);
            }

            output = output + line
          }

          fs.writeFile(fullPath, output, 'utf8', function(err) {
            if (err) return console.log(err);
          });
        });

        next()

      }
    });

    walker.on('end', function() {

    });
  }
  catch (e) {}
}

rewriteUrls()

//generatePages(controlFlowFn())

uploadFullSiteToS3(controlFlowFn())
